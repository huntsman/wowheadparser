using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Globalization;
namespace WindowsApplication1
{

	static class Brain
	{

		#region "Обьявления"
		private static int secp = 0;
		public static int Count;
		public static int MaxCount;
		public static int lastcount;
        private static List<drop> dropm = new List<drop>();
        private static List<dropp> droppm = new List<dropp>();
        private static List<drops> dropsm = new List<drops>();
        private static List<abil> abilm = new List<abil>();
        private static List<quest> questm = new List<quest>();
        private static List<selli> sellim = new List<selli>();
        private static List<tspells> tspellsm = new List<tspells>();
        private static List<npcmoney> npcmoneym = new List<npcmoney>();
		//Private Textemp As String
		private static int Errors;
		public static bool Pause;
		//Private WithEvents WebB As WebBrowser = New WebBrowser
		//Private WithEvents Timerp As Timer = New Timer

		public struct npcmoney
		{
			public int Npcid;
			public int money;
		}

		public struct quest
		{
			public int Npcid;
			public int Startid;
			public int StartClass;
			public int Endid;
			public int EndClass;
		}

		public struct drop
		{
			public int Npcid;
			public int id;
			public int count;
			public double chance;
		}

		public struct dropp
		{
			public int Npcid;
			public int id;
			public int count;
			public double chance;
		}

		public struct drops
		{
			public int Npcid;
			public int id;
			public int count;
			public double chance;
		}

		public struct abil
		{
			public int Si;
			public int Npcid;
			public int id;
		}

		public struct selli
		{
			public int Npcid;
			public int id;
			public int cost;
			public int slot;
		}

		public struct tspells
		{
			public int Npcid;
			public int id;
			public int cost;
			public int level;
			public int chrclass;
			public int regskill;
		}

		public enum ParseType
		{
			drops = 1,
			pickpocketing = 2,
			abilities = 3,
			starts = 4,
			//Квесты
			ends = 5,
			//Квесты
			sells = 6,
			teaches_ability = 7,
			skinning = 8,
			comments = 9,
			screenshots = 10,
			videos = 11
		}

		#endregion

		public static string downloadhtml(string url)
		{
			string htmltext = null;
			byte[] arr = null;
			WebClient client = new WebClient();
			client.Headers["User-Agent"] = "Mozilla/4.0";
			try {
				arr = client.DownloadData(url);
			} catch (Exception ex) {
				return "";
			}
			System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
			htmltext = enc.GetString(arr);

			return htmltext;
		}






		public static void insql()
		{
			int i = 0;
			int cou = 0;
			int cco = 0;
			string temp = "";

			if (npcmoneym.Count != 0) {
				for (i = 0; i <= npcmoneym.Count - 1; i++) {
                    if (npcmoneym[i].money != 0)
                    {
                        temp += "UPDATE `creature_template` SET `mingold`='" + npcmoneym[i].money + "', `maxgold`='" + npcmoneym[i].money + "' WHERE (`entry`='" + npcmoneym[i].Npcid + "');" + Constants.vbCrLf;
					}
				}
				SaveSqls(temp, "creature_loot_template_money");
				temp = "";
			}

			cco = 0;
			if (dropm.Count != 0) {
				for (i = 0; i <= dropm.Count - 1; i++) {
                    if (dropm[i].id != 0)
                    {
                        if (dropm[i].Npcid != cco)
                        {
                            temp += "DELETE FROM `creature_loot_template` WHERE (`entry`='" + dropm[i].Npcid + "');" + Constants.vbCrLf;
                            cco = dropm[i].Npcid;
						}

						temp += "INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('" + dropm[i].Npcid + "','" + dropm[i].id + "','" + Convert.ToDouble(dropm[i].chance).ToString(NumberFormatInfo.InvariantInfo) + "','1','0','1','2');" + Constants.vbCrLf;
					}
					if (i != dropm.Count - 1) {
						if (dropm[i].Npcid != dropm[i + 1].Npcid) {
							temp += "UPDATE `creature_template` SET `lootid`='" + dropm[i].Npcid + "' WHERE (`entry`='" + dropm[i].Npcid + "');" + Constants.vbCrLf;
						}
					} else {
						temp += "UPDATE `creature_template` SET `lootid`='" + dropm[i].Npcid + "' WHERE (`entry`='" + dropm[i].Npcid + "');" + Constants.vbCrLf;
					}
				}
				SaveSqls(temp, "creature_loot_template");
				temp = "";
			}
			cco = 0;
			if (droppm.Count != 0) {
				for (i = 0; i <= droppm.Count - 1; i++) {
					if (droppm[i].id != 0) {
						if (droppm[i].Npcid != cco) {
							temp += "DELETE FROM `pickpocketing_loot_template` WHERE (`entry`='" + droppm[i].Npcid + "');" + Constants.vbCrLf;
							cco = droppm[i].Npcid;
						}

						temp += "INSERT INTO `pickpocketing_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('" + droppm[i].Npcid + "','" + droppm[i].id + "','" + Convert.ToDouble(droppm[i].chance).ToString(NumberFormatInfo.InvariantInfo) + "','1','0','1','2');" + Constants.vbCrLf;
					}
					if (i != droppm.Count - 1) {
						if (droppm[i].Npcid != droppm[i + 1].Npcid) {
							temp += "UPDATE `creature_template` SET `pickpocketloot`='" + droppm[i].Npcid + "' WHERE (`entry`='" + droppm[i].Npcid + "');" + Constants.vbCrLf;
						}
					} else {
						temp += "UPDATE `creature_template` SET `pickpocketloot`='" + droppm[i].Npcid + "' WHERE (`entry`='" + droppm[i].Npcid + "');" + Constants.vbCrLf;
					}
				}
				SaveSqls(temp, "pickpocketing_loot_template");
				temp = "";
			}
			cco = 0;
			if (dropsm.Count != 0) {
				for (i = 0; i <= dropsm.Count - 1; i++) {
					if (dropsm[i].id != 0) {
						if (dropsm[i].Npcid != cco) {
							temp += "DELETE FROM `skinning_loot_template` WHERE (`entry`='" + dropsm[i].Npcid + "');" + Constants.vbCrLf;
							cco = dropsm[i].Npcid;
						}

						temp += "INSERT INTO `skinning_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES ('" + dropsm[i].Npcid + "','" + dropsm[i].id + "','" + Convert.ToDouble(dropsm[i].chance).ToString(NumberFormatInfo.InvariantInfo) + "','1','0','1','2');" + Constants.vbCrLf;
					}
					if (i != dropsm.Count - 1) {
						if (dropsm[i].Npcid != dropsm[i + 1].Npcid) {
							temp += "UPDATE `creature_template` SET `skinloot`='" + dropsm[i].Npcid + "' WHERE (`entry`='" + dropsm[i].Npcid + "');" + Constants.vbCrLf;
						}
					} else {
						temp += "UPDATE `creature_template` SET `skinloot`='" + dropsm[i].Npcid + "' WHERE (`entry`='" + dropsm[i].Npcid + "');" + Constants.vbCrLf;
					}
				}
				SaveSqls(temp, "skinning_loot_template");
				temp = "";
			}

			if (abilm.Count != 0) {

				for (i = 0; i <= abilm.Count - 1; i++) {
					if (abilm[i].id != 0) {
						temp += "UPDATE `creature_template` SET `spell" + abilm[i].Si + "`='" + abilm[i].id + "' WHERE (`entry`='" + abilm[i].Npcid + "');" + Constants.vbCrLf;
					}

				}
				SaveSqls(temp, "creature_template_Spells");
				temp = "";
			}
			cco = 0;
			if (tspellsm.Count != 0) {
				for (i = 0; i <= tspellsm.Count - 1; i++) {
					if (tspellsm[i].id != 0) {
						if (tspellsm[i].Npcid != cco) {
							temp += "DELETE FROM `npc_trainer` WHERE (`entry`='" + tspellsm[i].Npcid + "');" + Constants.vbCrLf;
							cco = tspellsm[i].Npcid;
						}

						temp += "INSERT INTO `npc_trainer` (`entry`,`spell`,`spellcost`,`reqskill`,`reqskillvalue`,`reqlevel`) VALUES ('" + tspellsm[i].Npcid + "','" + tspellsm[i].id + "','" + tspellsm[i].cost + "','" + tspellsm[i].regskill + "','0','" + tspellsm[i].level + "');" + Constants.vbCrLf;
					}
					if (i != tspellsm.Count - 1) {
						if (tspellsm[i].Npcid != tspellsm[i + 1].Npcid) {
							if (tspellsm[i].chrclass > 0 & tspellsm[i].chrclass < 12) {
								temp += "UPDATE `creature_template` SET `trainer_type` = '0', `trainer_class` = '" + tspellsm[i].chrclass + "' WHERE `entry` = '" + tspellsm[i].Npcid + "';" + Constants.vbCrLf;
							} else {
								temp += "UPDATE `creature_template` SET `trainer_type` = '0' WHERE `entry` = '" + tspellsm[i].Npcid + "';" + Constants.vbCrLf;
							}
						}
					} else {
						if (tspellsm[i].chrclass > 0 & tspellsm[i].chrclass < 12) {
							temp += "UPDATE `creature_template` SET `trainer_type` = '0', `trainer_class` = '" + tspellsm[i].chrclass + "' WHERE `entry` = '" + tspellsm[i].Npcid + "';" + Constants.vbCrLf;
						} else {
							temp += "UPDATE `creature_template` SET `trainer_type` = '0' WHERE `entry` = '" + tspellsm[i].Npcid + "';" + Constants.vbCrLf;
						}
					}
				}
				SaveSqls(temp, "npc_trainer");
				temp = "";
			}

			if (sellim.Count != 0) {
				for (i = 0; i <= sellim.Count - 1; i++) {
					if (sellim[i].id != 0) {
						temp += "REPLACE INTO `npc_vendor` (`entry`,`slot`,`item`,`maxcount`,`incrtime`,`ExtendedCost`) VALUES ('" + sellim[i].Npcid + "','0','" + sellim[i].id + "','0','0','0');" + Constants.vbCrLf;
						if (sellim[i].cost != 0) {
							temp += "UPDATE `item_template` SET `BuyPrice` = '" + sellim[i].cost + "' WHERE `entry` = '" + sellim[i].id + "';" + Constants.vbCrLf;
						}
					}
				}
				SaveSqls(temp, "npc_vendor");
				temp = "";
			}

			cou = 0;
			cco = 0;
			if (questm.Count != 0) {
				for (i = 0; i <= questm.Count - 1; i++) {
					if (questm[i].Startid != 0) {
						if (questm[i].Npcid != cou) {
							temp += "DELETE FROM `creature_questrelation` WHERE (`id`='" + questm[i].Npcid + "');" + Constants.vbCrLf;
							cou = questm[i].Npcid;
						}
						temp += "UPDATE `quest_template` SET `SkillOrClassMask`='" + questm[i].StartClass + "' WHERE (`entry`='" + questm[i].Startid + "');" + Constants.vbCrLf;
						temp += "REPLACE INTO `creature_questrelation` (`id`,`quest`) VALUES ('" + questm[i].Npcid + "','" + questm[i].Startid + "');" + Constants.vbCrLf;
					}
					if (questm[i].Endid != 0) {
						if (questm[i].Npcid != cco) {
							temp += "DELETE FROM `creature_involvedrelation` WHERE (`id`='" + questm[i].Npcid + "');" + Constants.vbCrLf;
							cco = questm[i].Npcid;
						}
						temp += "UPDATE `quest_template` SET `SkillOrClassMask`='" + questm[i].EndClass + "' WHERE (`entry`='" + questm[i].Endid + "');" + Constants.vbCrLf;
						temp += "REPLACE INTO `creature_involvedrelation` (`id`,`quest`) VALUES ('" + questm[i].Npcid + "','" + questm[i].Endid + "');" + Constants.vbCrLf;
					}
				}
				cou = 0;
				cco = 0;
				SaveSqls(temp, "creature_questr_inv");
				temp = "";
			}

			emptyarr();
		}

		public static void emptyarr()
		{
			npcmoneym.Clear();
			dropm.Clear();
			droppm.Clear();
			dropsm.Clear();
			abilm.Clear();
			tspellsm.Clear();
			sellim.Clear();
			questm.Clear();
		}


		public static void SaveSqls(string sqls, string name)
		{
			FileSystem.FileOpen(1, name + ".sql", OpenMode.Append);

			FileSystem.PrintLine(1, sqls);

			FileSystem.FileClose();
		}


		public static string AcInTeg(ref string Text, int left, string Right)
		{
			string Temp = "";
			int s = 0;
			if (left <= 0)
				left = 1;

			for (s = left; s <= Text.IndexOf(Right); s++) {
				Temp += Strings.Mid(Text, s, 1);
			}

			return Temp.Trim();
		}

		public static string AcInTeg(ref string Text, string left, string Right)
		{
			string Temp = "";
			int s = 0;
			if (Text.IndexOf(left) != -1) {
				for (s = Text.IndexOf(left) + Strings.Len(left) + 1; s <= Text.IndexOf(left) + Strings.Len(left) + 201; s++) {
					if (Strings.Mid(Text, s, 1) != Right) {
						Temp += Strings.Mid(Text, s, 1);
					} else {
						break; // TODO: might not be correct. Was : Exit For
					}


				}
			} else {
				Temp = "0";
			}

			return Temp.Trim();
		}

		public static string InTeg(ref string Text, string left, string Right)
		{
			string Temp = "";

			for (int s = Text.IndexOf(left) + Strings.Len(left) + 1; s <= Text.IndexOf(Right); s++) {
				Temp += Strings.Mid(Text, s, 1);

			}

			return Temp.Trim();
		}


		public static ParseType TypeRead(string text)
		{
			ParseType temp = default(ParseType);
			string te = null;
			te = AcInTeg(ref text, "id: '", "'");
			try {
				temp = (ParseType)Enum.Parse(typeof(ParseType), te);
			} catch {
			}

			if (te == "teaches-ability") {
				temp = ParseType.teaches_ability;
			}


			return temp;
		}

		public static string parseS(ref string S)
		{
			string temp = null;
			int ind = 0;
			int eind = 0;

			ind = S.IndexOf("new Listview") + Strings.Len("new Listview");
			S = S.Remove(0, S.IndexOf("new Listview") + Strings.Len("new Listview"));
			eind = S.IndexOf("new Listview");
			temp = S.Substring(0, eind);

			return temp;
		}

		#region "Readers"

		public static void moneyreader(string text)
		{
			npcmoney npcmoney = default(npcmoney);
			npcmoney.Npcid = Count;
			npcmoney.money = int.Parse(AcInTeg(ref text, "[money=", "]"));
			if (npcmoney.money > 0) {
				npcmoneym.Add(npcmoney);
			}
		}

		public static void dropreader(string text)
		{
			string temp = null;
			drop drop = default(drop);
			int dropc = 0;
			bool Fend = false;
			dropc = int.Parse(AcInTeg(ref text, "_totalCount: ", ","));
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				drop = new drop();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					drop.Npcid = Count;
					drop.id = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					//If text.IndexOf("""outof"":") = -1 Then
					//    drop.count = AcInTeg(text, "count:", ",")
					//End If
					if (AcInTeg(ref text, "\"classs\":", ",") == "12") {
                        drop.chance = -1 * Math.Round((double.Parse(AcInTeg(ref text, "\"count\":", ",")) / (dropc / 100)), 3);
					} else {
						drop.chance = Math.Round((double.Parse(AcInTeg(ref text, "\"count\":", ",")) / (dropc / 100)), 3);
					}
					dropm.Add(drop);
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}

		}

		public static void pickpreader(string text)
		{
			string temp = null;
			dropp drop = default(dropp);
			int dropc = 0;
			bool Fend = false;
			dropc = int.Parse(AcInTeg(ref text, "_totalCount: ", ","));
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				drop = new dropp();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					drop.Npcid = Count;
					drop.id = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					//If text.IndexOf("""outof"":") = -1 Then
					//    drop.count = AcInTeg(text, "count:", ",")
					//End If
					if (AcInTeg(ref text, "\"classs\":", ",") == "12") {
                        drop.chance = -1 * Math.Round((double.Parse(AcInTeg(ref text, "\"count\":", ",")) / (dropc / 100)), 3);
					} else {
                        drop.chance = Math.Round((double.Parse(AcInTeg(ref text, "count:", ",")) / (dropc / 100)), 3);
					}
					droppm.Add(drop);
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}

		}

		public static void skinreader(string text)
		{
			string temp = null;
			drops drop = default(drops);
			int dropc = 0;
			bool Fend = false;
			dropc = int.Parse(AcInTeg(ref text, "_totalCount: ", ","));
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				drop = new drops();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					drop.Npcid = Count;
					drop.id = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					//If text.IndexOf("""outof"":") = -1 Then
					//    drop.count = AcInTeg(text, "count:", ",")
					//End If
					if (AcInTeg(ref text, "\"classs\":", ",") == "12") {
                        drop.chance = -1 * Math.Round((double.Parse(AcInTeg(ref text, "\"count\":", ",")) / (dropc / 100)), 3);
					} else {
                        drop.chance = Math.Round((double.Parse(AcInTeg(ref text, "count:", ",")) / (dropc / 100)), 3);
					}
					dropsm.Add(drop);
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}

		}

		public static void abilreader(string text)
		{
			string temp = null;
			abil abil = default(abil);
			bool Fend = false;
			int ii = 0;
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				abil = new abil();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					ii += 1;
					abil.Npcid = Count;
					abil.id = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					abil.Si = ii;
					abilm.Add(abil);
					if (ii == 8)
						ii = 0;
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}

		}

		public static void sellreader(string text)
		{
			string temp = null;
			selli selli = default(selli);
			bool Fend = false;
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				selli = new selli();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					selli.Npcid = Count;
					selli.id = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					if (iscount(text)) {
						selli.cost = int.Parse(AcInTeg(ref text, "cost:[", "]"));
					}
					sellim.Add(selli);
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}

		}

		public static void qsreader(string text)
		{
			string temp = null;
			quest quest = default(quest);
			bool Fend = false;
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				quest = new quest();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					quest.Npcid = Count;
					quest.Startid = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					quest.StartClass = -1 * Convert.ToInt32(AcInTeg(ref text, "\"reqclass\":", ","));
					questm.Add(quest);
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}

		}

		public static int QClassDecode(int id)
		{
			string str = ((Qclass)id).ToString();
			if (str.IndexOf(",") != -1) {
				str = AcInTeg(ref str, 0, ",");
			}
			return (int)Enum.Parse(typeof(Cclass), str);
		}

		public enum Cclass
		{
			Warlock = 9,
			Priest = 5,
			Hunter = 3,
			Rogue = 4,
			DK = 6,
			Druid = 11,
			Shaman = 7,
			Mage = 8,
			Paladin = 2,
			Warrior = 1
		}

		[Flags()]
		public enum Qclass : int
		{
			Warrior = 1,
			Paladin = 2,
			//?
			Hunter = 4,
			Rogue = 8,
			Priest = 16,
			DK = 32,
			Shaman = 64,
			Warlock = 256,
			Mage = 128,
			Druid = 1024
		}

		public static void qereader(string text)
		{
			string temp = null;
			quest quest = default(quest);
			bool Fend = false;
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				quest = new quest();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					quest.Npcid = Count;
					quest.Endid = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					quest.EndClass = -1 * Convert.ToInt32(AcInTeg(ref text, "\"reqclass\":", ","));
					questm.Add(quest);
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}
		}

		public static void tareader(string text)
		{
			string temp = null;
			tspells tspells = default(tspells);
			bool Fend = false;
			text = text.Remove(0, text.IndexOf("data: [") + Strings.Len("data: ["));
			for (int i = 0; i <= text.Length; i++) {
				temp = "";
				tspells = new tspells();
				if (text.IndexOf("\"id\":") != -1) {
					temp = InTeg(ref text, "{", "},{");
					if (string.IsNullOrEmpty(temp)) {
						temp = InTeg(ref text, "{", "}]}");
						Fend = true;
					}
					tspells.Npcid = Count;
					tspells.id = int.Parse(AcInTeg(ref text, "\"id\":", ","));
					tspells.cost = int.Parse(AcInTeg(ref text, "\"trainingcost\":", "}"));
					tspells.level = int.Parse(AcInTeg(ref text, "\"level\":", ","));
					tspells.regskill = int.Parse(AcInTeg(ref text, "\"skill\":[", "]"));
					tspells.chrclass = QClassDecode(int.Parse(AcInTeg(ref text, "\"reqclass\":", ",")));
					tspellsm.Add(tspells);
					if (Fend == false) {
						text = text.Remove(0, text.IndexOf("},{") + Strings.Len("},"));
					} else {
						text = text.Remove(0, text.IndexOf("}]}") + Strings.Len("}]});"));
					}
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}
			}
		}

		#endregion



		//Private Sub WebB_DocumentCompleted(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles WebB.DocumentCompleted
		//    If e.Url.AbsolutePath <> sender.Url.AbsolutePath Then Return

		//End Sub



		public static void sleepwebn()
		{
			//WebB.Stop()
			webnav(Count);
		}

		public static void webnav(int count)
		{
			//WebB.Navigate("http://www.wowhead.com/npc=" & count)
			//downloadhtml("http://www.wowhead.com/npc=" & count)
		}

		public static bool iscount(string text)
		{
			int i = 0;
			int cot = 0;
			text = AcInTeg(ref text, "cost:[", "}");
			for (i = 0; i <= text.Length - 1; i++) {
				if (text.IndexOf("]") != -1) {
					cot += 1;
					text = text.Remove(0, text.IndexOf("]") + 1);
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}

			}
			if (cot < 2) {
				return true;
			} else {
				return false;
			}
		}

		public static int countn(string text)
		{
			int i = 0;
			int cot = 0;
			for (i = 0; i <= text.Length - 1; i++) {
				if (text.IndexOf("new Listview") != -1) {
					cot += 1;
					text = text.Remove(0, text.IndexOf("new Listview") + Strings.Len("new Listview"));
				} else {
					break; // TODO: might not be correct. Was : Exit For
				}

			}

			return cot;
		}


		//Private Sub Timerp_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timerp.Tick
		//    If Pause = False Then
		//        If secp < 21 Then
		//            secp += 1
		//        Else
		//            sleepwebn()
		//            secp = 0
		//        End If
		//    End If
		//End Sub

	}
}
