using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
namespace WindowsApplication1
{
	partial class Fform : System.Windows.Forms.Form
	{

		//Форма переопределяет dispose для очистки списка компонентов.
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Является обязательной для конструктора форм Windows Forms

		private System.ComponentModel.IContainer components;
		//Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
		//Для ее изменения используйте конструктор форм Windows Form.  
		//Не изменяйте ее в редакторе исходного кода.
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.BRun = new System.Windows.Forms.Button();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.Label1 = new System.Windows.Forms.Label();
            this.CountBox = new System.Windows.Forms.TextBox();
            this.MCountBox = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.Timer2 = new System.Windows.Forms.Timer(this.components);
            this.Label7 = new System.Windows.Forms.Label();
            this.UpdBox = new System.Windows.Forms.TextBox();
            this.BPause = new System.Windows.Forms.Button();
            this.Label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BRun
            // 
            this.BRun.Location = new System.Drawing.Point(93, 261);
            this.BRun.Name = "BRun";
            this.BRun.Size = new System.Drawing.Size(75, 23);
            this.BRun.TabIndex = 1;
            this.BRun.Text = "Начать";
            this.BRun.UseVisualStyleBackColor = true;
            this.BRun.Click += new System.EventHandler(this.BRun_Click);
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(6, 211);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(162, 33);
            this.ProgressBar1.TabIndex = 3;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label1.Location = new System.Drawing.Point(6, 145);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(57, 63);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "0";
            // 
            // CountBox
            // 
            this.CountBox.Location = new System.Drawing.Point(44, 11);
            this.CountBox.Name = "CountBox";
            this.CountBox.Size = new System.Drawing.Size(100, 20);
            this.CountBox.TabIndex = 5;
            this.CountBox.Text = "0";
            // 
            // MCountBox
            // 
            this.MCountBox.Location = new System.Drawing.Point(44, 48);
            this.MCountBox.Name = "MCountBox";
            this.MCountBox.Size = new System.Drawing.Size(100, 20);
            this.MCountBox.TabIndex = 6;
            this.MCountBox.Text = "50000";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(13, 18);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(23, 13);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "От:";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(11, 51);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(25, 13);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "До:";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label5.Location = new System.Drawing.Point(66, 299);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(42, 46);
            this.Label5.TabIndex = 11;
            this.Label5.Text = "0";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label6.ForeColor = System.Drawing.Color.Maroon;
            this.Label6.Location = new System.Drawing.Point(12, 345);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(152, 29);
            this.Label6.TabIndex = 14;
            this.Label6.Text = "Дамп череЗ";
            // 
            // Timer1
            // 
            this.Timer1.Interval = 1000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // Timer2
            // 
            this.Timer2.Interval = 1000;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(3, 132);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(13, 13);
            this.Label7.TabIndex = 15;
            this.Label7.Text = "0";
            // 
            // UpdBox
            // 
            this.UpdBox.Location = new System.Drawing.Point(44, 87);
            this.UpdBox.Name = "UpdBox";
            this.UpdBox.Size = new System.Drawing.Size(100, 20);
            this.UpdBox.TabIndex = 16;
            this.UpdBox.Text = "30";
            // 
            // BPause
            // 
            this.BPause.Enabled = false;
            this.BPause.Location = new System.Drawing.Point(6, 261);
            this.BPause.Name = "BPause";
            this.BPause.Size = new System.Drawing.Size(75, 23);
            this.BPause.TabIndex = 17;
            this.BPause.Text = "Дамп";
            this.BPause.UseVisualStyleBackColor = true;
            this.BPause.Click += new System.EventHandler(this.BPause_Click);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(13, 71);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(103, 13);
            this.Label4.TabIndex = 18;
            this.Label4.Text = "Сохранять каждые";
            // 
            // Fform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(171, 378);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.BPause);
            this.Controls.Add(this.UpdBox);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.MCountBox);
            this.Controls.Add(this.CountBox);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.BRun);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Fform";
            this.Text = "Wry Wowhead Parser";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        internal System.Windows.Forms.Button BRun;
		internal System.Windows.Forms.ProgressBar ProgressBar1;
		internal System.Windows.Forms.Label Label1;
		internal System.Windows.Forms.TextBox CountBox;
		internal System.Windows.Forms.TextBox MCountBox;
		internal System.Windows.Forms.Label Label2;
		internal System.Windows.Forms.Label Label3;
		internal System.Windows.Forms.Label Label5;
		internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Timer Timer1;
		internal System.Windows.Forms.Timer Timer2;
		internal System.Windows.Forms.Label Label7;
		internal System.Windows.Forms.TextBox UpdBox;
        internal System.Windows.Forms.Button BPause;
		internal System.Windows.Forms.Label Label4;
	}
}
