using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
namespace WindowsApplication1
{
	public partial class Fform
	{
		private int sec;
		private Thread FileThread;
		public delegate void addinfd(int cas);
		public delegate void readerd(string text);
		public delegate void Oops();



		public void start()
		{
			Brain.Count = int.Parse(CountBox.Text);
			Brain.MaxCount = int.Parse(MCountBox.Text);
			//ProgressBar1.Maximum = MaxCount
			sec = int.Parse(UpdBox.Text);
			//WebB.ScriptErrorsSuppressed = True
			//Timerp.Interval = 1000

			//webnav(Count)
            if (FileThread != null)
                if (FileThread.IsAlive)
                {
                    FileThread.Resume();
                }
                else
                    FileThread.Start();
            else
            {
                FileThread = new Thread(ParseP);
                FileThread.Start();
            }
			Timer1.Start();
			//Timerp.Start()
		}

        protected void ParseP()
		{
			string textm = null;
			string tt = null;
			string ttt = null;
			int i = 0;
			int ii = 0;
			//Dim tpm As npc
			//For ii = Count To MaxCount
			do {
                if (Brain.Pause == false)
                {
                    textm = "";
                    textm = Brain.downloadhtml("http://www.wowhead.com/npc=" + Brain.Count);
                    //if (textm != "")
                    //{
                        readerd moneyreaderd = new readerd(Brain.moneyreader);
                        this.Invoke(moneyreaderd, textm);
                        //moneyreader(textm)
                        ttt = Brain.InTeg(ref textm, "<h2 class=\"clear\">Related</h2>", "<h2 class=\"clear\">Contribute</h2>");


                        if (textm.IndexOf("new Listview") != -1)
                        {

                            for (i = 0; i <= Brain.countn(ttt) - 2; i++)
                            {
                                tt = Brain.parseS(ref ttt);
                                //If teaches-ability
                                switch (Brain.TypeRead(tt))
                                {
                                    case Brain.ParseType.drops:
                                        readerd dropreaderd = new readerd(Brain.dropreader);
                                        //dropreader(tt)
                                        this.Invoke(dropreaderd, tt);
                                        break;
                                    case Brain.ParseType.pickpocketing:
                                        readerd pickpreaderd = new readerd(Brain.pickpreader);
                                        this.Invoke(pickpreaderd, tt);
                                        break;
                                    //pickpreader(tt)
                                    case Brain.ParseType.abilities:
                                        readerd abilreaderd = new readerd(Brain.abilreader);
                                        this.Invoke(abilreaderd, tt);
                                        break;
                                    //abilreader(tt)
                                    case Brain.ParseType.starts:
                                        readerd qsreaderd = new readerd(Brain.qsreader);
                                        this.Invoke(qsreaderd, tt);
                                        break;
                                    //qsreader(tt)
                                    case Brain.ParseType.ends:
                                        readerd qereaderd = new readerd(Brain.qereader);
                                        this.Invoke(qereaderd, tt);
                                        break;
                                    //qereader(tt)
                                    case Brain.ParseType.sells:
                                        readerd sellreaderd = new readerd(Brain.sellreader);
                                        this.Invoke(sellreaderd, tt);
                                        break;
                                    //sellreader(tt)
                                    case Brain.ParseType.skinning:
                                        readerd skinreaderd = new readerd(Brain.skinreader);
                                        this.Invoke(skinreaderd, tt);
                                        break;
                                    //skinreader(tt)
                                    case Brain.ParseType.teaches_ability:
                                        readerd tareaderd = new readerd(Brain.tareader);
                                        this.Invoke(tareaderd, tt);
                                        break;
                                    //tareader(tt)
                                }


                            }
                        }



                        //Dim cancelDelegat As New DownloadCompleteSafe(AddressOf textadd)

                        Oops addinfd = new Oops(addinf);

                        if (Brain.Count <= Brain.MaxCount)
                        {
                            this.Invoke(addinfd);
                            //addinf()

                            if (Brain.Count == Brain.MaxCount)
                            {
                                Oops textaddd = new Oops(textadd);
                                this.Invoke(textaddd);
                                //textadd()
                            }


                            if (Brain.Count < Brain.MaxCount)
                            {
                                Brain.Count += 1;

                                //webnav(Count)


                            }

                        }
                        //secp = 0
                    }
                    else
                    {
                        Oops insqld = new Oops(Brain.insql);
                        this.Invoke(insqld);
                        //insql()
                        Brain.Count += 1;
                        Brain.Pause = false;
                        //webnav(Count)
                    }
			} while (true);
			//Next ii
		}

		public void addinf()
		{
			if (Brain.Count != Brain.MaxCount) {
				ProgressBar1.Value = Conversion.Int(Brain.Count * 100 / Brain.MaxCount);
			}
			Label1.Text = Convert.ToString(Brain.Count);
		}

		public void textadd()
		{
			FileThread.Suspend();
			Brain.insql();
			ProgressBar1.Value = 0;
			Label1.Text = Convert.ToString(0);
			Timer1.Stop();
		}

		private void Fform_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
		{
            if (FileThread != null)
			if (FileThread.IsAlive) {
				Brain.Pause = true;
				FileThread.Resume();
				FileThread.Abort();
			}



		}


		public Fform()
		{
			FormClosing += Fform_FormClosing;
			InitializeComponent();
		}

        private void BRun_Click(object sender, EventArgs e)
        {
            start();
        }

        private void BPause_Click(object sender, EventArgs e)
        {
			Brain.Pause = true;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
			if (Brain.Pause == false) {
				if (sec > 0) {
					sec -= 1;
					Label5.Text = Convert.ToString(sec);
				} else {
					sec = int.Parse(UpdBox.Text);
					Brain.Pause = true;
					Label5.Text = Convert.ToString(sec);
				}
			}
        }

	}
}
